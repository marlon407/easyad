angular.module('PageService', []).factory('Page', function() {
   var title = 'EasyAd';
   return {
     title: function() { return title; },
     setTitle: function(newTitle) { title = newTitle }
   };
});